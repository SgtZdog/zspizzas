using System;
using System.Collections.Generic;
using DataZsPizzas.Types;
using TypesZsPizzas.Interfaces;
using TypesZsPizzas.Types;

namespace DataZsPizzas
{
    [Serializable]
    public class BusinessDayTable : IBusinessDayTable
    {
        private List<DataBusinessDay> _businessDays = new List<DataBusinessDay>();
        public IReadOnlyCollection<BusinessDay> BusinessDays => _businessDays.AsReadOnly();
    }
}