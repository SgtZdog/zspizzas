using System;
using System.Collections.Generic;
using DataZsPizzas.Types;
using TypesZsPizzas.Interfaces;
using TypesZsPizzas.Types;

namespace DataZsPizzas
{
    [Serializable]
    public class CustomerTable : ICustomerTable
    {
        private List<DataCustomer> _customers = new List<DataCustomer>();
        public IReadOnlyCollection<Customer> Customers => _customers.AsReadOnly();
    }
}