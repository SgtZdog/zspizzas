using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using DataZsPizzas.Types;
using TypesZsPizzas.Interfaces;
using TypesZsPizzas.Types;

namespace DataZsPizzas
{
    [Serializable]
    public class EmployeeTable : IEmployeeTable
    {
        private List<DataEmployee> _employees = new List<DataEmployee> {new DataEmployee(0, 0, Employee.EmployeeType.Manager, "Initial", "Sample")};
        private const string defaultSaveLocation = "Dat";
        private const string defaultSaveName = "EmployeeTable.dat";

        public EmployeeTable ()
        {
            var fullPath = Path.Combine(defaultSaveLocation, defaultSaveName);
            if (File.Exists(fullPath))
            {
                using var dataStream = new FileStream(fullPath, FileMode.Open);
                var binaryFormatter = new BinaryFormatter();
                _employees = ((EmployeeTable)binaryFormatter.Deserialize(dataStream))._employees;
            }
        }

        ~EmployeeTable ()
        {
            WriteToDisk();
        }

        private void WriteToDisk ()
        {
            Directory.CreateDirectory(defaultSaveLocation);
            using var dataStream = new FileStream(Path.Combine(defaultSaveLocation, defaultSaveName), FileMode.Create);
            var binaryFormatter = new BinaryFormatter();
            binaryFormatter.Serialize(dataStream, this);
        }

        public void AddEmployee (Employee employee)
        {
            var idCandidate = (uint)Employees.Count;
            while (Employees.Any(existingEmployee => existingEmployee.ID == idCandidate))
            {
                idCandidate++;
            }
            _employees.Add(
                           new DataEmployee(
                                            idCandidate,
                                            employee.Compensation,
                                            employee._EmployeeType,
                                            employee.FirstName,
                                            employee.LastName));
            
            WriteToDisk();
        }

        public IReadOnlyCollection<Employee> Employees => _employees.AsReadOnly();

        public Employee GetEmployeeByID (uint id)
        {
            return Employees.FirstOrDefault(candidate => candidate.ID == id);
        }

        public Employee GetEmployeeByName (string firstName, string lastName)
        {
            return Employees.FirstOrDefault(candidate => candidate.FirstName == firstName && candidate.LastName == lastName);
        }

        public void UpdateEmployee (Employee employee)
        {
            var targetEmployee = _employees.First(employeeCandidate => employee.ID == employeeCandidate.ID);
            targetEmployee.FirstName = employee.FirstName;
            targetEmployee.LastName = employee.LastName;
            targetEmployee.Enabled = employee.Enabled;
            targetEmployee.Compensation = employee.Compensation;
            targetEmployee._EmployeeType = employee._EmployeeType;
            
            WriteToDisk();
        }

        public void DeleteEmployee (uint id)
        {
            _employees.Remove(_employees.First(employee => employee.ID == id));
            
            WriteToDisk();
        }
    }
}