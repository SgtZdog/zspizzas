using System;
using TypesZsPizzas.Interfaces;

namespace DataZsPizzas
{
    [Serializable]
    public class PizzaDatabase
    {
        public IEmployeeTable EmployeeTable { get; private set; } = new EmployeeTable();

        public ICustomerTable CustomerTable { get; private set; } = new CustomerTable();

        public IBusinessDayTable BusinessDayTable { get; private set; } = new BusinessDayTable();
    }
}