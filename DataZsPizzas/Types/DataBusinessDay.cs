using System;
using TypesZsPizzas.Types;

namespace DataZsPizzas.Types
{
    [Serializable]
    public class DataBusinessDay : BusinessDay
    {
        internal DataBusinessDay () { }

        internal DataBusinessDay (BusinessDay businessDay)
            : base(businessDay) { }
    }
}