using System;
using TypesZsPizzas.Types;

namespace DataZsPizzas.Types
{
    [Serializable]
    public class DataCustomer : Customer
    {
        internal DataCustomer (ulong phoneNumber, string firstName, string lastName)
            : base(phoneNumber, firstName, lastName) { }

        internal DataCustomer (Customer customer)
            : base(customer) { }

        public void UpdateName (string firstName = null, string lastName = null)
        {
            if (firstName != null) FirstName = firstName;
            if (lastName != null) LastName = lastName;
        }
    }
}