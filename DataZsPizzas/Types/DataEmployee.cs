using System;
using TypesZsPizzas.Types;

namespace DataZsPizzas.Types
{
    [Serializable]
    public class DataEmployee : Employee
    {
        internal DataEmployee (uint id, float Compensation, EmployeeType EmployeeType, string FirstName, string LastName)
            : base(id, Compensation, EmployeeType, FirstName, LastName) { }

        internal DataEmployee (Employee employee)
            : base(employee) { }
    }
}