using System;
using TypesZsPizzas.Types;

namespace DataZsPizzas.Types
{
    [Serializable]
    public class DataEmployeeShift : EmployeeShift
    {
        internal DataEmployeeShift (EmployeeShift employeeShift)
            : base(employeeShift) { }
    }
}