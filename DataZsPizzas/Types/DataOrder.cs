using System;
using TypesZsPizzas.Interfaces;
using TypesZsPizzas.Types;

namespace DataZsPizzas.Types
{
    [Serializable]
    public class DataOrder : Order
    {
        internal DataOrder (uint id, Customer Customer, IAddress Address, PizzaType PizzaType, PizzaSize PizzaSize)
            : base(id, Customer, Address, PizzaType, PizzaSize) { }

        internal DataOrder (Order order)
            : base(order) { }
    }
}