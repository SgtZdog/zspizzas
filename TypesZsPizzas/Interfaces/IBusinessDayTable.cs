using System.Collections.Generic;
using TypesZsPizzas.Types;

namespace TypesZsPizzas.Interfaces
{
    public interface IBusinessDayTable
    {
        public IReadOnlyCollection<BusinessDay> BusinessDays { get; }
    }
}