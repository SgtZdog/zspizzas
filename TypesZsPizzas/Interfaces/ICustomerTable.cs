using System.Collections.Generic;
using TypesZsPizzas.Types;

namespace TypesZsPizzas.Interfaces
{
    public interface ICustomerTable
    {
        public IReadOnlyCollection<Customer> Customers { get; }
    }
}