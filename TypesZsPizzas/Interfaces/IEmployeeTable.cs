using System.Collections.Generic;
using TypesZsPizzas.Types;

namespace TypesZsPizzas.Interfaces
{
    public interface IEmployeeTable
    {
        void AddEmployee (Employee employee);
        IReadOnlyCollection<Employee> Employees { get; }
        Employee GetEmployeeByID (uint ID);
        Employee GetEmployeeByName (string firstName, string lastName);
        void UpdateEmployee (Employee employee);
        void DeleteEmployee (uint id);
    }
}