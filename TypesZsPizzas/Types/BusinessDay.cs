using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using TypesZsPizzas.Interfaces;

namespace TypesZsPizzas.Types
{
    [Serializable]
    public abstract class BusinessDay
    {
        public readonly DateTime Date;

        private readonly List<EmployeeShift> _shifts = new List<EmployeeShift>();
        public ReadOnlyCollection<EmployeeShift> Shifts => _shifts.AsReadOnly();

        protected readonly List<Order> _orders = new List<Order>();
        public ReadOnlyCollection<Order> Orders => _orders.AsReadOnly();

        protected BusinessDay ()
        {
            Date = DateTime.Today;
        }

        protected BusinessDay (BusinessDay businessDay)
        {
            Date = businessDay.Date;
            _shifts.AddRange(businessDay.Shifts);
            _orders.AddRange(businessDay.Orders);
        }

        protected void AddEmployeeShift<TEmployeeShift> (Employee employee) where TEmployeeShift : EmployeeShift
        {
            if (_shifts.Count is 0 && !(employee._EmployeeType is Employee.EmployeeType.Manager))
            {
                throw new InvalidOperationException(
                                                    $"First employee must be of {nameof(Employee.EmployeeType.Manager)}.");
            }

            if (_shifts.Exists(shift => shift.Employee == employee && shift.EndTime is null))
            {
                throw new InvalidOperationException($"Employee {employee.FirstName} already has an open shift.");
            }

            if (typeof(TEmployeeShift).IsAbstract)
            {
                throw new ArgumentException($"{nameof(TEmployeeShift)} must not be abstract!");
            }

            _shifts.Add((TEmployeeShift)Activator.CreateInstance(typeof(TEmployeeShift), employee));
        }

        protected void AddOrder (Order order)
        {
            if (!Orders.Contains(order) && Orders.All(existingOrder => existingOrder.ID != order.ID))
            {
                _orders.Add(order);
            }
        }

        protected void AddOrder<TOrder> (
            Customer Customer,
            IAddress Address,
            Order.PizzaType PizzaType,
            Order.PizzaSize PizzaSize)
            where TOrder : Order
        {
            if (typeof(TOrder).IsAbstract)
            {
                throw new ArgumentException($"{nameof(TOrder)} must not be abstract!");
            }

            var id = Orders.Count;
            while (Orders.Any(existingOrder => existingOrder.ID == id))
            {
                id++;
            }
            AddOrder((TOrder)Activator.CreateInstance(typeof(TOrder), id, Customer, Address, PizzaType, PizzaSize));
        }
    }
}