using System;

namespace TypesZsPizzas.Types
{
    [Serializable]
    public abstract class Customer
    {
        public readonly ulong PhoneNumber;
        
        public string FirstName { get; protected set; }
        
        public string LastName { get; protected set; }

        protected Customer (ulong phoneNumber, string firstName, string lastName)
        {
            PhoneNumber = phoneNumber;
            FirstName = firstName;
            LastName = lastName;
        }

        protected Customer (Customer customer)
        {
            PhoneNumber = customer.PhoneNumber;
            FirstName = customer.FirstName;
            LastName = customer.LastName;
        }
    }
}