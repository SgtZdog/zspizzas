using System;
using System.ComponentModel.DataAnnotations;

namespace TypesZsPizzas.Types
{
    [Serializable]
    public abstract class Employee
    {
        public uint ID { get; set; }
        
        public bool Enabled { get; set; } = true;
        
        public float Compensation { get; set; }

        [Required(ErrorMessage = "First name is required.", AllowEmptyStrings = false), 
         StringLength(30, MinimumLength = 1, ErrorMessage = "Invalid First name.")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Last name is required.", AllowEmptyStrings = false), 
         StringLength(30, MinimumLength = 1, ErrorMessage = "Invalid Last name.")]
        public string LastName { get; set; }
        
        public enum EmployeeType
        {
            Cook,
            Driver,
            Manager
        }
        [Display(Name = "Employee Type")]
        public EmployeeType _EmployeeType { get; set; }

        protected Employee (uint id, float compensation, EmployeeType employeeType, string firstName, string lastName)
        {
            ID = id;
            Compensation = compensation;
            _EmployeeType = employeeType;
            FirstName = firstName;
            LastName = lastName;
        }

        protected Employee (Employee employee)
        {
            ID = employee.ID;
            Enabled = employee.Enabled;
            Compensation = employee.Compensation;
            FirstName = employee.FirstName;
            LastName = employee.LastName;
            _EmployeeType = employee._EmployeeType;
        }

        protected Employee () { }
    }
}