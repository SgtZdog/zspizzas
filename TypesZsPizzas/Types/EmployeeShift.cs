using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace TypesZsPizzas.Types
{
    [Serializable]
    public abstract class EmployeeShift
    {
        public readonly Employee Employee;

        public readonly DateTime StartTime;

        public DateTime? EndTime { get; protected set; }

        private readonly List<Order> _ordersAssigned = new List<Order>();

        public ReadOnlyCollection<Order> OrdersAssigned => _ordersAssigned.AsReadOnly();

        internal EmployeeShift (Employee Employee)
        {
            this.Employee = Employee;
            StartTime = DateTime.Now;
        }

        protected EmployeeShift (EmployeeShift employeeShift)
        {
            Employee = employeeShift.Employee;
            StartTime = employeeShift.StartTime;
            EndTime = employeeShift.EndTime;
            _ordersAssigned = employeeShift._ordersAssigned;
        }

        protected internal void AssignOrder (Order order)
        {
            if (_ordersAssigned.Contains(order))
            {
                throw new InvalidOperationException("Order already assigned to employee!");
            }
            _ordersAssigned.Add(order);
        }
    }
}