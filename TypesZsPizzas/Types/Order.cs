using System;
using TypesZsPizzas.Interfaces;

namespace TypesZsPizzas.Types
{
    [Serializable]
    public abstract class Order
    {
        public readonly uint ID;

        public readonly Customer Customer;
        
        public readonly IAddress Address;
        
        public enum PizzaType
        {
            Hawaiian,
            BBQ,
            Supreme
        }
        public readonly PizzaType _PizzaType;
        
        public enum PizzaSize
        {
            Small,
            Medium,
            Large
        }
        public readonly PizzaSize _PizzaSize;
        
        public enum OrderStatus
        {
            Received,
            Preparing,
            Baking,
            Delivering,
            Delivered
        }

        public OrderStatus _OrderStatus { get; private set; } = OrderStatus.Received;

        public Employee AssignedEmployee { get; private set; }

        public readonly DateTime OrderedTime;

        public DateTime? DeliveredTime { get; protected set; }

        public float? Tip { get; protected set; }

        protected internal Order (uint id, Customer customer, IAddress address, PizzaType pizzaType, PizzaSize pizzaSize)
        {
            ID = id;
            Customer = customer;
            Address = address;
            _PizzaType = pizzaType;
            _PizzaSize = pizzaSize;
            OrderedTime = DateTime.Now;
        }

        protected internal Order (Order order)
        {
            Customer = order.Customer;
            Address = order.Address;
            _PizzaType = order._PizzaType;
            _OrderStatus = order._OrderStatus;
            AssignedEmployee = order.AssignedEmployee;
            OrderedTime = order.OrderedTime;
            DeliveredTime = order.DeliveredTime;
            Tip = order.Tip;
        }

        protected void UpdateStatus (OrderStatus orderStatus, EmployeeShift EmployeeShift = null)
        {
            string errorMessage = default;
            switch (orderStatus)
            {
                case OrderStatus.Received:
                    errorMessage = $"Order should not be re-marked as {nameof(OrderStatus.Received)}.";
                    break;
                case OrderStatus.Preparing:
                    if (!(_OrderStatus is OrderStatus.Received))
                    {
                        errorMessage = $"Order should only be set to {nameof(OrderStatus.Preparing)} from {nameof(OrderStatus.Received)}.";
                    }
                    break;
                case OrderStatus.Baking:
                    if (!(_OrderStatus is OrderStatus.Received))
                    {
                        errorMessage = $"Order should only be set to {nameof(OrderStatus.Baking)} from {nameof(OrderStatus.Preparing)}.";
                    }
                    break;
                case OrderStatus.Delivering:
                    if (!(_OrderStatus is OrderStatus.Received))
                    {
                        errorMessage = $"Order should only be set to {nameof(OrderStatus.Delivering)} from {nameof(OrderStatus.Baking)}.";
                    }
                    if (EmployeeShift?.EndTime is null || !(EmployeeShift.Employee._EmployeeType is Employee.EmployeeType.Driver))
                    {
                        errorMessage += $"Order must have a {nameof(Employee.EmployeeType.Driver)} on shift to mark as {nameof(OrderStatus.Delivering)}.";
                    }
                    break;
                case OrderStatus.Delivered:
                    if (!(_OrderStatus is OrderStatus.Received))
                    {
                        errorMessage = $"Order should only be set to {nameof(OrderStatus.Delivered)} from {nameof(OrderStatus.Delivering)}.";
                    }
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(orderStatus), orderStatus, null);
            }

            if (errorMessage is null)
            {
                _OrderStatus = orderStatus;
                AssignedEmployee = EmployeeShift?.Employee;
                EmployeeShift?.AssignOrder(this);
            }
            else
            {
                throw new InvalidOperationException(errorMessage);
            }
        }
    }
}