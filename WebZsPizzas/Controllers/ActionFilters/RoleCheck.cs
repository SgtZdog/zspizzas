using System.IO;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using TypesZsPizzas.Types;
using WebZsPizzas.Utility;

namespace WebZsPizzas.Controllers.ActionFilters
{
    public class RoleCheck : ActionFilterAttribute
    {
        private readonly string _redirect;
        private readonly Employee.EmployeeType[] _targetRoles;

        /// <summary>
        ///     Take in the redirect route and valid roles
        ///     WILL CAUSE UNEXPECTED BEHAVIOR IF REDIRECT IS FORGOTTEN
        /// </summary>
        /// <param name="redirect"></param>
        /// <param name="targetRoles"></param>
        public RoleCheck (string redirect, params Employee.EmployeeType[] targetRoles)
        {
            _redirect = redirect;
            _targetRoles = targetRoles;
        }

        /// <summary>
        ///     Compare current RoleName to valid role names and redirect if no match
        /// </summary>
        /// <param name="filterContext"></param>
        public override void OnActionExecuting (ActionExecutingContext filterContext)
        {
            if (filterContext.HttpContext.Session.TryGetValue(SessionKeys.User, out var employee))
            {
                using var memoryStream = new MemoryStream(employee);
                var role = ((Employee)Formatter.BinaryFormatter.Deserialize(memoryStream))._EmployeeType;
                if (_targetRoles.All(validRole => validRole != role))
                {
                    filterContext.Result = new RedirectResult(_redirect);
                }
            }
            else
            {
                filterContext.Result = new RedirectResult(_redirect);
            }

            base.OnActionExecuting(filterContext);
        }
    }
}