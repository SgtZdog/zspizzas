using System;
using DataZsPizzas;
using Microsoft.AspNetCore.Mvc;

namespace WebZsPizzas.Controllers
{
    public abstract class BaseController : Controller
    {
        protected static readonly PizzaDatabase _pizzaDatabase = new PizzaDatabase(); 
        
        protected const string DefaultRedirect = "~/Home/Index";
        
        protected static void WriteErrorToConsole (Exception e)
        {
            if (e is null) throw new ArgumentNullException(nameof(e));
            Console.WriteLine(e);
        }
    }
}