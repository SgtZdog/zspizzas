using System;
using Microsoft.AspNetCore.Mvc;
using TypesZsPizzas.Types;
using WebZsPizzas.Controllers.ActionFilters;
using WebZsPizzas.Models;
using WebZsPizzas.Utility;

namespace WebZsPizzas.Controllers
{
    public class UserController : BaseController
    {
        // GET
        public IActionResult Index ()
        {
            return RedirectToAction("Login");
        }
        
        [HttpGet]
        public IActionResult Login ()
        {
            return View();
        }
        
        [HttpPost]
        public ActionResult Login (EmployeeVM login)
        {
            //Return variable
            ActionResult result;

            try
            {
                if (ModelState.IsValid)
                {
                    var databaseResult = _pizzaDatabase.EmployeeTable.GetEmployeeByName(login.FirstName, login.LastName);
                    if (databaseResult is null)
                    {
                        ViewBag.Message = "First and/or Last name is incorrect!";
                        result = View(login);
                    }
                    else
                    {
                        var user = new EmployeeVM(databaseResult);
                        HttpContext.Session.Set(SessionKeys.User, Formatter.Serialize(user));
                        result = RedirectToAction("Index", "Home");
                    }
                }
                else //ModelState not valid
                {
                    result = View(login);
                }
            }
            catch (Exception e)
            {
                //Error occurred, return to user with message.
                WriteErrorToConsole(e);
                ViewBag.Message = e.Data[ExceptionDataKeys.Problem] ?? e;
                result = View(login);
            }

            return result;
        }

        public IActionResult Logout ()
        {
            HttpContext.Session.Remove(SessionKeys.User);
            return RedirectToAction("Index", "Home");
        }
        
        [HttpGet, RoleCheck(DefaultRedirect, Employee.EmployeeType.Manager)]
        public ActionResult UpdateEmployee (uint id)
        {
            //Return Variable
            ActionResult result;
            ViewBag.Message = TempData[TempDataKeys.Message];
            try
            {
                var employeeVM = new EmployeeVM(_pizzaDatabase.EmployeeTable.GetEmployeeByID(id));
                result = View(employeeVM);
            }
            catch (Exception e)
            {
                //Error occurred, redirect back to search with message
                WriteErrorToConsole(e);
                TempData[TempDataKeys.Message] = $"Error finding user {id}.";
                result = RedirectToAction("DisplayEmployees");
            }

            return result;
        }
        
        [HttpPost, RoleCheck(DefaultRedirect, Employee.EmployeeType.Manager)]
        public ActionResult UpdateEmployee (EmployeeVM employee)
        {
            //Return Variable
            ActionResult result;
            if (ModelState.IsValid)
                try
                {
                    _pizzaDatabase.EmployeeTable.UpdateEmployee(employee);
                    //Redirect back when done
                    result = RedirectToAction("DisplayEmployees");
                }
                catch (Exception e)
                {
                    //Error occurred, return with message.
                    WriteErrorToConsole(e);
                    ViewBag.Message = e.Data[ExceptionDataKeys.Problem] ?? e;
                    result = View(employee.FirstName);
                }
            else //ModelState is not valid
                result = View(employee.FirstName);

            return result;
        }

        [HttpGet, RoleCheck(DefaultRedirect, Employee.EmployeeType.Manager)]
        public IActionResult DisplayEmployees ()
        {
            return View(_pizzaDatabase.EmployeeTable.Employees);
        }

        [HttpPost, RoleCheck(DefaultRedirect, Employee.EmployeeType.Manager)]
        public IActionResult CreateEmployee (EmployeeVM employee)
        {
            _pizzaDatabase.EmployeeTable.AddEmployee(employee);
            return RedirectToAction("DisplayEmployees");
        }
    }
}