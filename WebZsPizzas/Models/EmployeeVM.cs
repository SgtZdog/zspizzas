using System;
using TypesZsPizzas.Types;

namespace WebZsPizzas.Models
{
    [Serializable]
    public class EmployeeVM : Employee
    {
        public EmployeeVM(){}
        
        private EmployeeVM (uint id, float compensation, EmployeeType employeeType, string firstName, string lastName)
            : base(id, compensation, employeeType, firstName, lastName) { }

        public EmployeeVM (Employee employee)
            : base(employee) { }
    }
}