using System;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

namespace WebZsPizzas.Utility
{
    public static class Formatter
    {
        public static readonly IFormatter BinaryFormatter = new BinaryFormatter();

        public static byte[] Serialize<TSerializable> (TSerializable subject) where TSerializable : new()
        {
            if (!typeof(TSerializable).IsSerializable)
            {
                throw new ArgumentException($"{nameof(TSerializable)} is not Serializable!");
            }
            using var memoryStream = new MemoryStream();
            BinaryFormatter.Serialize(memoryStream, subject);
            return memoryStream.ToArray();
        }

        public static TSerializable Deserialize<TSerializable> (byte[] subject) where TSerializable : new()
        {
            if (!typeof(TSerializable).IsSerializable)
            {
                throw new ArgumentException($"{nameof(TSerializable)} is not Serializable!");
            }
            using var memoryStream = new MemoryStream(subject);
            return (TSerializable)BinaryFormatter.Deserialize(memoryStream);
        }
    }
}